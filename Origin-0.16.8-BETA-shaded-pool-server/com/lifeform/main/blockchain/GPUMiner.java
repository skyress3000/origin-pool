package com.lifeform.main.blockchain;

import com.lifeform.main.IKi;
import com.lifeform.main.data.EncryptionManager;
import com.lifeform.main.data.Utils;
import com.lifeform.main.network.BlockEnd;
import com.lifeform.main.network.BlockHeader;
import com.lifeform.main.network.TransactionPacket;
import gpuminer.JOCL.constants.JOCLConstants;
import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.JOCL.context.JOCLContextMaster;
import gpuminer.JOCL.context.JOCLDevices;
import gpuminer.miner.SHA3.SHA3Miner;
import gpuminer.miner.autotune.Autotune;
import gpuminer.miner.context.ContextMaster;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.databuckets.BlockAndSharePayloads;
import gpuminer.miner.databuckets.Payload;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;

import static org.jocl.CL.CL_DEVICE_TYPE_GPU;
import static org.jocl.CL.stringFor_cl_device_address_info;

public class GPUMiner extends Thread implements IMiner {

    private IKi ki;
    private int index;
    public static volatile boolean mining = false;

    private Block b = null; //this is the current block

    private SHA3Miner miner;
    private DeviceContext jcacq;
    private static List<SHA3Miner> gpuMiners = new ArrayList<>();
    private static List<DeviceContext> jcacqs_;
    private static volatile boolean autotuneDone = false;
    private static volatile boolean stopAutotune = false;
    private static volatile boolean triedNoCPU = false;
    public static ContextMaster platforms = new ContextMaster();
    public static boolean initDone = false;
    public static BigInteger minFee = BigInteger.TEN;
    public static int init(IKi ki) throws MiningIncompatibleException {
        //You have to shut these down when you're done with them.
        for (SHA3Miner m : gpuMiners) {
            //This takes the place of stopAndClear() for putting the SHA3Miner and its SHA3MinerThread object in an unrecoverable state.
            //It will completely kill the thread that interacts with the GPU.
            m.shutdown();
        }
        gpuMiners.clear();
        //Gotta shut this down, too, and after the SHA3Miner objects are shut down.
        if (platforms != null) {
            platforms.shutdown();
        }
        platforms = new ContextMaster();
        List<DeviceContext> jcacqs = platforms.getContexts();
        final Thread t = new Thread() {

            public void run() {
                ki.debug("Starting autotune");
                Autotune.setup(jcacqs, false);
                autotuneDone = true;

            }
        };
        t.start();
        while (!autotuneDone) {
            //ki.debug("Autotune not done");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        /*ki.debug("Autotune done");
        if (stopAutotune && !triedNoCPU) {
            ki.debug("Autotune was stopped");
            if (!triedNoCPU) {
                triedNoCPU = true;
                JOCLDevices.setDeviceFilter(CL_DEVICE_TYPE_GPU);
                return init(ki);
            }
        } else */
        /*
        if (stopAutotune) {
            throw new MiningIncompatibleException("Autotune took more than 5 minutes, your device may be compatible, but is running so slowly that it would not be profitable to mine on.");
        }
        */



        byte[] difficulty = new byte[64];
        int p = 63;
        for (int i = ki.getChainMan().getCurrentDifficulty().toByteArray().length - 1; i >= 0; i--) {
            difficulty[p] = ki.getChainMan().getCurrentDifficulty().toByteArray()[i];
            p--;
        }
        int i = 0;
        for (DeviceContext jcaq : jcacqs) {
            int threadCount = Autotune.getAtSettingsMap().get(jcaq.getDInfo().getDeviceName()).threadFactor;
            SHA3Miner miner = new SHA3Miner(jcaq, difficulty, null, threadCount, Autotune.getAtSettingsMap().get(jcaq.getDInfo().getDeviceName()).kernelType);
            gpuMiners.add(miner);
            i++;

        }

        jcacqs_ = jcacqs;
        initDone = true;
        return jcacqs.size();
    }

    private long hashrate = -1;

    public GPUMiner(IKi ki, int index) {
        this.ki = ki;
        this.devName = devName + " #" + index;

    }

    private long lastPrint = System.currentTimeMillis();
    private DecimalFormat format = new DecimalFormat("###,###,###,###");
    private int threadCount;

    @Override
    public void run() {
        if(!ki.getPoolMan().isRunning()) ki.getPoolMan().doServer(); //instead of normal mining, just run the server if it's not running
    }

    @Override
    public void interrupt() {
        miner.pauseMining();
        super.interrupt();
    }

    @Override
    public long getHashrate() {
        return hashrate;
    }
    private boolean disabled = false;
    private String devName;
    @Override
    public void setup(int index) {
        miner = gpuMiners.get(index);
        jcacq = jcacqs_.get(index);
        devName = jcacq.getDInfo().getDeviceName() + " #" + index;
        disabled = !ki.getMinerMan().getDevNames().contains(devName);
    }
}