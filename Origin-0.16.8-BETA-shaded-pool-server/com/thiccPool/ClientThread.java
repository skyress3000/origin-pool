package com.thiccPool;

import java.net.*;
import java.io.*;

import com.lifeform.main.IKi;
import com.lifeform.main.blockchain.Block;

public class ClientThread extends Thread {
	private int totalHashes = 0;
	private double unwithdrawnOrigin = 0.0; //this will wait until it's 10 to send to client
	private String name = "";
	private String address = "";
	private Socket socket = null;
	private PoolServerMan serverMan;
	private IKi ki;
	//ki.getAddMan().getMainAdd().encodeForChain() how to turn address to string

	public ClientThread(String name, Socket socket, IKi ki, PoolServerMan serverMan) {
		super(name);
		this.name = name;
		this.socket = socket;
		this.serverMan = serverMan;
		this.ki = ki;
	}

	public void resetHashes() {
		totalHashes = 0;
	}

	public int getHashes() {
		return totalHashes;
	}

	public String getAddress() {
		return address;
	}

	public double getOrigin() {
		return unwithdrawnOrigin;
	}

	public void addOrigin(double amt) {
		unwithdrawnOrigin += amt;
	}

	public void withdrawOigin(double amt) {
		unwithdrawnOrigin -= amt;
	}

	public void run() {
		try (
            OutputStream out = socket.getOutputStream();
            PrintWriter printOut = new PrintWriter(socket.getOutputStream(), true);
            InputStream rawIn = socket.getInputStream();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        ) {
            ki.debug("connected to " + name);

            String inputLine;
            boolean foundBlock = false;
            while(true) {
            	//this while loop VVV will run whenever the client sends something
                while ((inputLine = in.readLine()) != null) {
                    if(inputLine.equals("blockRequest")) {
                    	Block b = serverMan.getCurrentBlock(); //send current block to client
                        printOut.println(b.toJSON());
                    }
                    if(inputLine.equals("hereComesAShare")) {
                        ki.debug("recieving a share"); //recieve a share (in form of JSON block) and process it

                        Block shareBlock = Block.fromJSON(in.readLine());
                        totalHashes += 1;

                        foundBlock = serverMan.processShare(shareBlock);
                    }
                    if(inputLine.equals("infoRequest")) {
                    	//send some generic info to the client
                    	printOut.println("Name: " + name + " | Shares mined on current block: " + totalHashes + " | Pending origin (will be sent once it reaches 10): " + unwithdrawnOrigin + (foundBlock ? " | YOU FOUND A BLOCK LAST SHARE" : ""));
                    	foundBlock = false;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port or listening for a connection");
            System.out.println(e.getMessage());
        }
	}
}