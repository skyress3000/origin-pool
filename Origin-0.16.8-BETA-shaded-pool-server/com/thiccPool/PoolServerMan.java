package com.thiccPool;

import com.lifeform.main.blockchain.Block;
import com.lifeform.main.blockchain.IChainMan;
import com.lifeform.main.data.EncryptionManager;
import com.lifeform.main.data.Utils;
import com.lifeform.main.network.BlockEnd;
import com.lifeform.main.network.BlockHeader;
import com.lifeform.main.network.TransactionPacket;
import com.lifeform.main.IKi;

import java.math.BigInteger;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.ArrayList;

import gpuminer.miner.databuckets.Payload;

public class PoolServerMan {
	private IKi ki;
	private boolean isRunning = false; //used to prevent doServer being called multiple times
	private List<ClientThread> clients;
	private Block currentBlock;
	private int sharesThisBlock = 0;

    public static BigInteger minFee = BigInteger.TEN;

	public PoolServerMan(IKi ki) {
		this.ki = ki;
		clients = new ArrayList<ClientThread>();
	}

	public boolean isRunning() {
		return isRunning;
	}

	//this is the main method in this class
	public void doServer() {
		isRunning = true;
        int portNumber = 4455;

        currentBlock = ki.getChainMan().formEmptyBlock(minFee);

        ki.debug("waiting for client connection");
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) { 
            while (true) {
            	//accept waits until a new connection is made
            	Socket clientSocket = serverSocket.accept();
            	ki.debug("found a client, waiting for name...");
            	String name = null;
            	//get name from client
            	try {
            		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		            String inputLine;
		            while(name == null) {
		                while ((inputLine = in.readLine()) != null) {
		                    name = inputLine;
		                    break;
		                }
		            }
		            ki.debug("name " + name + " recieved");
		        } catch (IOException e) {
		            System.out.println("Exception caught when trying to listen on port "
		                + portNumber + " or listening for a connection");
		            System.out.println(e.getMessage());
		        }
		        //connect to client and let thread deal with it
		        ClientThread client = new ClientThread(name, clientSocket, ki, this);
	            clients.add(client);
	            client.start();
	        }
	    } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
	}

	private void sendBlock(Block b) {
		//same as sendBlock in GPUMiner
        if (ki.getOptions().mDebug)
            ki.debug("Sending block to network from miner");
        BlockHeader bh2 = formHeader(b);
        ki.getNetMan().broadcast(bh2);


        for (String key : b.getTransactionKeys()) {
            TransactionPacket tp = new TransactionPacket();
            tp.block = b.ID;
            tp.trans = b.getTransaction(key).toJSON();
            ki.getNetMan().broadcast(tp);
        }
        BlockEnd be = new BlockEnd();
        be.ID = b.ID;
        ki.getNetMan().broadcast(be);
        if (ki.getOptions().mDebug)
            ki.debug("Done sending block");
    }

    private BlockHeader formHeader(Block b) {
    	//same as formHeader in GPUMiner
        BlockHeader bh = new BlockHeader();
        bh.timestamp = b.timestamp;
        bh.solver = b.solver;
        bh.prevID = b.prevID;
        bh.payload = b.payload;
        bh.merkleRoot = b.merkleRoot;
        bh.ID = b.ID;
        bh.height = b.height;
        bh.coinbase = b.getCoinbase().toJSON();
        return bh;
    }

    //gives the current block, only makes a new one if the height has changed since the last request
    public Block getCurrentBlock() {
    	if(!ki.getChainMan().currentHeight().equals(currentBlock.height)) {
    		sharesThisBlock = 0;
    		for(ClientThread thread : clients) {
    			thread.resetHashes(); //new block so no shares anymore
    		}
    		currentBlock = ki.getChainMan().formEmptyBlock(minFee);
    	}
    	return currentBlock;
    }

    public boolean processShare(Block shareBlock) {
    	boolean isBlock = false;
    	shareBlock.ID = EncryptionManager.sha512(shareBlock.header());

        if (ki.getChainMan().softVerifyBlock(shareBlock).success()) {
            ki.getChainMan().setTemp(shareBlock); //this is basically the same as block processing for the normal client
            sendBlock(shareBlock);
            ki.debug("YOU FOUND A BLOCK! If it verifies you will receive it back shortly from the network.");
            for (String t : shareBlock.getTransactionKeys()) {
                ki.getTransMan().getPending().remove(shareBlock.getTransaction(t));
            }
            isBlock = true;
        } else if (ki.getChainMan().getCurrentDifficulty().compareTo(new BigInteger(Utils.fromBase64(shareBlock.ID))) < 0) {
            //this will happen fairly often because only 1/shareDifficulty shares is a block
            ki.debug("the share is not a block my DUDE");
            sharesThisBlock += 1;
            isBlock = false;
        } else {
            ki.debug("An error was found with the block verification. Block will not be sent to the network");
        }
        return isBlock;
    }
}