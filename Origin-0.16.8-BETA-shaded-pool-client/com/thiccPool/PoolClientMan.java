package com.thiccPool;

import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Scanner;

import com.lifeform.main.IKi;
import com.lifeform.main.blockchain.Block;

import gpuminer.miner.databuckets.Payload;

public class PoolClientMan {
    private Socket server = null;
    private Block currentBlock = null;
    private IKi ki; 

	public PoolClientMan(IKi ki) {
		//connect to pool server
        String hostName = "24.240.108.246";
        int portNumber = 4455;

        ki.debug("Connecting to server...");
        try {
            server = new Socket(hostName, portNumber);
            ki.debug("Connected to server");
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }

        try {
            PrintWriter out = new PrintWriter(server.getOutputStream(), true);
            String name = getName();
            out.println(name);
            ki.debug("Sent name: " + name);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("server IO machine broke");
            System.exit(1);
        }

        this.ki = ki;
	}

	public byte[] getCurrentHeader() {
        //gets the current gpuHeader for mining on
        try {
            PrintWriter out = new PrintWriter(server.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
            out.println("blockRequest");
            ki.debug("waiting for block from server....");
            boolean gotBlock = false;
            while(!gotBlock) {
                if(in.ready()) {
                    String blockJSON = in.readLine();
                    //ki.debug("recieved block:" + blockJSON);
                    //just sending the header was making it tweak so now it just gets the block in JSON and gets the header out of that
                    currentBlock = Block.fromJSON(blockJSON);
                    gotBlock = true;
                }
            }
            ki.debug("received block successfully");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("server IO machine broke");
            System.exit(1);
        }
        return currentBlock.gpuHeader();
	}

	public void sendShare(Payload pl) {
		try {
            PrintWriter out = new PrintWriter(server.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
            out.println("blockRequest");
            ki.debug("waiting for block from server....");
            boolean gotBlock = false;
            Block b = null;
            while(!gotBlock) {
                if(in.ready()) {
                    String blockJSON = in.readLine();
                    //ki.debug("recieved block:" + blockJSON);
                    //again, sending raw byte arrays was tweaking, so we grab the server's block in JSON, inject the payload, and ship it back
                    b = Block.fromJSON(blockJSON);
                    gotBlock = true;
                }
            }
            b.payload = pl.getBytes();
            out.println("hereComesAShare");
            //byte[] payloadLengthBytes = ByteBuffer.allocate(4).putInt(pl.getBytes().length).array();
            //out.write(payloadLengthBytes);
            //out.write(pl.getBytes()); this is the stuff that was tweaking
            out.println(b.toJSON());
            out.println("infoRequest"); //get some general updates from the server whenever we find a share
            boolean infoRecieved = false;
            while(!infoRecieved) {
                if(in.ready()) {
                    ki.debug("info from server: " + in.readLine());
                    infoRecieved = true;
                }
            }
        } catch (IOException e) {
            System.err.println("server IO machine broke");
            System.exit(1);
        }
	}

    public String getName() {
        Scanner sc = null;
        try {
            File file = new File("name.txt");
            sc = new Scanner(file);
            return sc.nextLine();
        } catch (Exception e) {
            System.out.println("Error reading name.txt");
            System.exit(1);
        } finally {
            if(sc != null) {
                sc.close();
            }
        }

        return null;
    }
}